module.exports = {
  env: {
    commonjs: true,
    es2021: true,
    node: true,
    mocha: true
  },
  extends: ['eslint:recommended', 'standard', 'plugin:chai-friendly/recommended'],
  plugins: [
    'chai-friendly'
  ],
  parserOptions: {
    ecmaVersion: 12
  },
  rules: {
  }
}
