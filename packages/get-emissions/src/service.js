const DB = require('@vizzuality-challenge/db')

const defaultQueryParams = {
  year_start: 1850,
  year_end: 2014,
  min_value: Number.MIN_VALUE,
  max_value: Number.MAX_VALUE,
  sort_key: 'country',
  sort_dir: 'asc',
  page: 0,
  page_size: 100
}

/** @typedef {import('mongoose').FilterQuery<import('@vizzuality-challenge/db/src/models/Emission').EmissionDocument>[]} EmissionFilterQuery  */

async function service (queryParams) {
  const params = { ...defaultQueryParams, ...queryParams }

  /** @type {EmissionFilterQuery} */
  const and = [
    { year: { $gte: params.year_start } },
    { year: { $lte: params.year_end } },
    { value: { $gte: params.min_value } },
    { value: { $lte: params.max_value } }
  ]

  if (params.country) {
    and.push({ country: { $eq: params.country } })
  }

  if (params.sector) {
    and.push({ sector: { $eq: params.sector } })
  }

  if (params.parent_sector) {
    and.push({ parentSector: { $eq: params.parent_sector } })
  }

  const data = await DB.Models.Emission
    .find({ $and: and })
    .sort({ [params.sort_key]: params.sort_dir })
    .skip(params.page * params.page_size)
    .limit(params.page_size)

  return { data }
}

module.exports = { service }
