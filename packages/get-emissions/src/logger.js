const LoggerFactory = require('@vizzuality-challenge/logger')

const logger = LoggerFactory.buildLogger(
  process.env.NODE_ENV === 'production' ? 'info' : 'debug',
  [{ enabled: true, type: 'console' }]
)

module.exports = { logger }
