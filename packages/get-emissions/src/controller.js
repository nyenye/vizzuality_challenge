const { service } = require('./service')

/**
 * @function
 * @description Returns an array of matching emissions.
 * Example query: localhost:3000/api/emissions?year_start=1950&year_end=1960&min_value=0.1&max_value=0.5&country=AND&page=2&page_size=50
 * @param {import("express").Request} req
 * @param {import("express").Response} res
 * @param {import("express").NextFunction} next
 */
async function controller (req, res, next) {
  try {
    const response = await service(req.query)
    return res.status(200).json(response.data).send()
  } catch (err) {
    return next(err)
  }
}

module.exports = { controller }
