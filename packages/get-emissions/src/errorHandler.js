const { logger } = require('./logger')

/**
 * @param {Error} err
 * @param {import('express').Request} req
 * @param {import('express').Response} res
 * @param {import('express').NextFunction} next
 */
function errorHandler (err, req, res, next) {
  logger.info(JSON.stringify({ headers: req.headers, params: req.params, query: req.query, body: req.body }))

  if (err instanceof Error) {
    logger.error(err.message)
    logger.error(err.stack)
  } else {
    logger.error(err)
  }

  return res.status(500).send()
}

module.exports = {
  errorHandler
}
