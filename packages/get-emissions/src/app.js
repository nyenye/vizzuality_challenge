const Express = require('express')

const { controller } = require('./controller')
const { errorHandler } = require('./errorHandler')

const app = Express()

app.use(controller, errorHandler)

module.exports = { app }
