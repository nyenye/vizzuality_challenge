const process = require('process')

const DB = require('@vizzuality-challenge/db')

const { app } = require('./src/app')
const { logger } = require('./src/logger')

async function init () {
  await DB.connect()
  process.on('beforeExit', async () => {
    await DB.close()
  })

  app.listen(process.env.HTTP_SERVER_PORT, () => {
    logger.info(`Listening on localhost:${process.env.HTTP_SERVER_PORT}`)
  })
}

init()
