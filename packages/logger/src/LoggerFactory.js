const winston = require('winston')

/** @typedef {'console' | 'file' | 'http' | 'stream'} TransportType */
/** @typedef {winston.transports.ConsoleTransportOptions | winston.transports.FileTransportOptions | winston.transports.HttpTransportOptions | winston.transports.StreamTransportOptions} TransportOptions */
/**
 * @typedef {Object} TransportConfig
 * @property {TransportType} type
 * @property {boolean} enabled
 * @property {TransportOptions} [options=undefined]
*/
/** @typedef {'error' | 'warn' | 'info' | 'http' | 'verbose' | 'debug' | 'silly'} LoggerLevel */

/**
 * @class
 * @description LoggerFactory class with static methods to build logger instances
 */
function LoggerFactory () {
  /**
   * @function
   * @description Instanciates a transport with matching type
   * @param {TransportConfig} transportConfig
   * @returns {winston.transport | null}
  */
  function buildTransport (transportConfig) {
    if (!transportConfig.enabled) {
      return null
    }

    if (transportConfig.type === 'console') {
      return new winston.transports.Console(transportConfig.options)
    }

    if (transportConfig.type === 'file') {
      return new winston.transports.File(transportConfig.options)
    }

    if (transportConfig.type === 'http') {
      return new winston.transports.Http(transportConfig.options)
    }

    if (transportConfig.type === 'stream') {
      return new winston.transports.Http(transportConfig.options)
    }

    return null
  }

  this._private = {
    buildTransport
  }
}

/**
 * @method
 * @description Builds an instance of winston.Logger given some configurations
 *
 * @param {LoggerLevel} [level='silly']
 * @param {Array<TransportConfig>} [transportConfigs=[]]
 * @param {boolean} [silent=false]
 *
 * @returns {winston.Logger}
*/
LoggerFactory.prototype.buildLogger = function (level = 'silly', transportConfigs = [], silent = false) {
  const transports = transportConfigs.map((transportConfig) => {
    return this._private.buildTransport(transportConfig)
  }).filter((transport) => transport !== null)

  if (transports.length === 0) {
    throw new Error('No valid transports')
  }

  return winston.createLogger({ level, transports, silent })
}

module.exports = new LoggerFactory()
