const Express = require('express')

const { controller } = require('./controller')
const { middlewares } = require('./middlewares')
const { errorHandler } = require('./errorHandler')

const app = Express()

app.use(...middlewares, controller, errorHandler)

module.exports = { app }
