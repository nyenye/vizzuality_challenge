const multer = require('multer')
const path = require('path')
const { v4: uuid4 } = require('uuid')

const storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, path.join(__dirname, '..', 'tmp'))
  },
  filename: function (req, file, cb) {
    const filename = `${uuid4()}.csv`
    cb(null, filename)
  }
})

const upload = multer({ storage })

module.exports = {
  middlewares: [
    upload.single('csv_file')
  ]
}
