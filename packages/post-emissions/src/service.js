const fs = require('fs')
const CSV = require('csvtojson')

const DB = require('@vizzuality-challenge/db')

async function service (pathToCsvFile) {
  return new Promise((resolve, reject) => {
    const bulk = DB.Models.Emission.collection.initializeOrderedBulkOp()

    CSV().fromStream(fs.createReadStream(pathToCsvFile)).subscribe((data) => {
      const { Country: country, Sector: sector, 'Parent sector': parentSector, ...yearlyReadings } = data

      const years = Object.keys(yearlyReadings)
      for (let j = 0; j < years.length; j++) {
        const year = Number(years[j])
        const value = Number(yearlyReadings[year])
        bulk.insert({ country, sector, parentSector, year, value })
      }
    }).on('done', async (error) => {
      if (error) {
        reject(error)
      }

      await bulk.execute()
      await fs.promises.unlink(pathToCsvFile)

      resolve()
    }).on('error', async (error) => {
      await fs.promises.unlink(pathToCsvFile)

      reject(error)
    })
  })
}

module.exports = { service }
