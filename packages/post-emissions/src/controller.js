const { service } = require('./service')

/**
 * @function
 * @param {import("express").Request} req
 * @param {import("express").Response} res
 * @param {import("express").NextFunction} next
 */
async function controller (req, res, next) {
  if (req.file === undefined) {
    return next(new Error('CSV file missing'))
  }

  try {
    await service(req.file.path)
    return res.send()
  } catch (error) {
    // TODO: Propperly log this error
    return next(error)
  }
}

module.exports = { controller }
