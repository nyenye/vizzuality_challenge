
const { Schema, model } = require('mongoose')

/* eslint-disable no-unused-vars */
const { Document, Model } = require('mongoose')
/* eslint-enable no-unused-vars */

/**
 * @typedef {Document} EmissionDocument
 * @property {string} country
 * @property {string} sector
 * @property {string} parentSector
 * @property {number} year
 * @property {number} value
*/

/** @typedef {Model<EmissionDocument>} EmissionModel */

/**
 * @class
 * @description This class encapsules the mongoose model for the Emission document
*/
function Emission () {
  /** @type {EmissionModel} */
  this._model = null

  const schema = new Schema(
    {
      country: Schema.Types.String,
      sector: Schema.Types.String,
      parentSector: Schema.Types.String,
      year: Schema.Types.Number,
      value: Schema.Types.Number
    }
  )

  this._model = model('Emission', schema)
}

module.exports = { Emission }
