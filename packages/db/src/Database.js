const process = require('process')
const { connect } = require('mongoose')

/* eslint-disable no-unused-vars */
const { Connection } = require('mongoose')
/* eslint-enable no-unused-vars */

const Models = require('./models')

/**
 * @typedef {Object} IModels
 * @property {import('./models/Emission').EmissionModel} Emission
*/

/**
 * @class
 * @description This class encapsules the Database connection and the mongoose models
 */
function Database () {
  /** @type {Connection} */
  this._db = null

  /** @type {IModels} */
  this._models = null

  this._models = {
    Emission: new Models.Emission()._model
  }
}

Database.prototype.connect = async function () {
  const mongoose = await connect(String(process.env.MONGO_URI))

  this._db = mongoose.connection
}

module.exports = { Database }
