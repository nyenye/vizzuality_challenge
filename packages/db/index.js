const { Database } = require('./src/Database')

/** @type {Database} */
let instance = null

module.exports = {
  get Models () {
    return instance._models
  },
  connect: async function () {
    if (instance !== null) {
      return
    }

    instance = new Database()
    await instance.connect()

    console.log('Mongoose has connected')
  },
  close: async function () {
    await instance._db.close()
    instance = null

    console.log('Mongoose has disconnected')
  }
}
